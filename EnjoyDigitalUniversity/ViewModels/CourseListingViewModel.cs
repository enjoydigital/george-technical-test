﻿using System.Collections.Generic;
using System.Globalization;
using EnjoyDigitalUniversity.Models;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace EnjoyDigitalUniversity.ViewModels
{
    public class CourseListingViewModel : RenderModel
    {
        public CourseListingViewModel(IPublishedContent content, CultureInfo culture) : base(content, culture)
        {
        }

        public CourseListingViewModel(IPublishedContent content) : base(content)
        {
        }

        public IEnumerable<CourseViewModel> Courses { get; set; }

        public string Department { get; set; }

        public IEnumerable<string> Departments { get; set; }
    }    
}