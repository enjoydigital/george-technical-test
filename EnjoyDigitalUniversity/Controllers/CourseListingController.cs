﻿using System.Linq;
using System.Web.Mvc;
using EnjoyDigitalUniversity.ViewModels;
using Umbraco.Core;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace EnjoyDigitalUniversity.Controllers
{
    public class CourseListingController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var courseListing = new CourseListingViewModel(CurrentPage);

            var allTypes = Umbraco.DataTypeService.GetAllDataTypeDefinitions();
            var typeId = allTypes.First(x => "Department".InvariantEquals(x.Name)).Id;
            var departments = Umbraco.DataTypeService.GetPreValuesByDataTypeId(typeId);

            TryUpdateModel(courseListing);

            courseListing.Departments = departments;

            return CurrentTemplate(courseListing);
        }
    }
}